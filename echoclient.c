#include "csapp.h"
#include<stdio.h>
#include<unistd.h>
#include<string.h>
int main(int argc, char **argv)
{
	int clientfd,last;
	char *port, *filename;
	char *host, buf[MAXLINE];
	rio_t rio;
	/*FILE *fp;*/

	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port> <filename>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	filename = argv[3];	
	last=strlen(filename);
	filename[last]='\n';
	filename[last+1]='\0';
	clientfd = Open_clientfd(host, port);
	
	Rio_readinitb(&rio, clientfd);
	Rio_writen(clientfd, filename, strlen(filename));
	
	sleep(5);
	
	Rio_readlineb(&rio, buf,100);
	if(strcmp(buf,"-1\n")==0){
		printf("File not found");
	}
	else{
		printf("File received: %s bytes",buf);
	}
	/*
	fp = Fopen(filename, "w+");
	Fputs(buf);
	Fclose(fp);
	*/
	Close(clientfd);
	exit(0);
}
