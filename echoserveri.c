#include "csapp.h"
#include <stdlib.h>
#include <time.h>
#include<string.h>
void echo(int connfd);
void send_file(int,int);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s) ", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE],file[MAXLINE];
    char size[MAXLINE];
	rio_t rio;
    struct stat sbuf;
	Rio_readinitb(&rio, connfd);
	n = Rio_readlineb(&rio, buf, MAXLINE);
    printf("archivo: %s ",buf);
    printf("server received %lu  bytes ",n);
    strncpy(file,buf,strlen(buf)-1);
    printf("%s",file);
    printf("%d",stat(buf,&sbuf));
    if(stat(file,&sbuf)==0){
	printf(" --Founded File! ");
        sprintf(size,"%ld",sbuf.st_size);
        Rio_writen(connfd,size,sbuf.st_size);
    }
    else{
	printf(" --404 ");
        Rio_writen(connfd,"-1\n",strlen("-1\n"));
    }
   /* Rio_writen(connfd,buf,strlen(buf));*/
}

void send_file(int connfd,int filesize){
/**/
}
